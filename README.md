# Otto
Otto theme for KDE Plasma


![title](https://cdn.pling.com/img/1/d/a/d/26f262c33ea4c14afc05bb40fd1997056c86.png)


Theme (KDE Store) : https://store.kde.org/p/1358260/ <br>
Source : https://www.opencode.net/jomada/otto

## Installation 
 * Download from [source](https://www.opencode.net/jomada/otto)
 * Install Kvantum
 * File List (dir): 
    * Color Scheme  : `color-schemes `
    * Kvantum Theme : `kvantum`
    * Desktop Theme : `Otto`
    * Konsole Theme : `konsole`
    * Latte Layout  : `Latte layout`
    * Look And Feel : `look-and-feel`
    * Wallpaper     : `Otto.png`
  * Copy And replace those file `~/.local/share/`


For More themes ~ https://www.opencode.net/jomada <br>
For Info about plasma themes ~ https://techbase.kde.org/Development/Tutorials/Plasma5/Theme